package nz.co.decodedhealth.vonage

import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

/** VonageChannelPlugin */
class VonageChannelPlugin : FlutterPlugin, MethodCallHandler {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private lateinit var channel: MethodChannel
    private val vonageListener = VonageListener()

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "VonageChannelPlugin")
        channel.setMethodCallHandler(this)

        vonageListener.attachNativeView(flutterPluginBinding)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        if (call.method == "initSession") {
            result.success(initSession(call))
        } else if (call.method == "endSession") {
            result.success(vonageListener.endSession())
        } else if (call.method == "publishStream") {
            result.success(publishStream(call))
        } else if (call.method == "unpublishStream") {
            result.success(vonageListener.unPublishStream())
        } else {
            result.notImplemented()
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

    private fun initSession(@NonNull call: MethodCall): String {
        val sessionId = call.argument<String?>("sessionId")
        val token = call.argument<String?>("token")
        val apiKey = call.argument<String?>("apiKey")
        vonageListener.initializeSession(
                sessionId = sessionId!!,
                token = token!!,
                apiKey = apiKey!!
        )
        return ""
    }

    private fun publishStream(@NonNull call: MethodCall): String {
        val sessionId = call.argument<String?>("sessionId")
        val token = call.argument<String?>("token")
        val apiKey = call.argument<String?>("apiKey")
        val name = call.argument<String?>("name")

        vonageListener.publishStream(
                sessionId = sessionId!!,
                token = token!!,
                apiKey = apiKey!!,
                name = name!!
        )

        return ""
    }

}
