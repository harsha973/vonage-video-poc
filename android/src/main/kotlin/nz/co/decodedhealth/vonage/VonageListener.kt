package nz.co.decodedhealth.vonage

import android.content.Context
import android.util.Log
import com.opentok.android.*
import io.flutter.embedding.engine.plugins.FlutterPlugin
import nz.co.decodedhealth.vonage.NativeViewFactory

internal class VonageListener : Session.SessionListener, PublisherKit.PublisherListener, SubscriberKit.SubscriberListener {
    private val LOG_TAG = "VonageVideoPlugin"
    private var session: Session? = null
    private var publisher: Publisher? = null
    private var subscriber: Subscriber? = null
    private var nativeView = NativeViewFactory()
    private lateinit var context: Context

    fun attachNativeView(binding: FlutterPlugin.FlutterPluginBinding) {
        context = binding.applicationContext
        binding.platformViewRegistry.registerViewFactory("flutter-vonage-video-publisher", nativeView)
    }

    /*** SessionListener LISTENER ***/
    override fun onConnected(session: Session) {
        Log.i(LOG_TAG, "Session Connected")
        doPublish()
    }

    override fun onDisconnected(session: Session) {
        Log.i(LOG_TAG, "Session Disconnected")
    }

    override fun onStreamReceived(session: Session, stream: Stream) {
        Log.i(LOG_TAG, "session Stream Received")
        if (subscriber == null) {
            Log.i(LOG_TAG, "Creating subscriber")
            subscriber = Subscriber.Builder(context, stream).build()
            subscriber?.renderer?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL)
            subscriber?.setSubscriberListener(this) // triggers onConnected(subscriber: SubscriberKit) once subscribed
            session.subscribe(subscriber)
        }
        Log.i(LOG_TAG, "session Stream Received END")
    }

    override fun onStreamDropped(session: Session, stream: Stream) {
        Log.i(LOG_TAG, "Stream Dropped")
        // cleanup publisher and subscriber when session stream dropped
        cleanupSubscriber()
        cleanupPublisher()
    }

    override fun onError(session: Session, opentokError: OpentokError) {
        Log.e(LOG_TAG, "Session error: " + opentokError.message)
    }

    /*** PublisherListener LISTENER ***/
    override fun onStreamCreated(publisherKit: PublisherKit, stream: Stream) {
        Log.i(LOG_TAG, "Publisher onStreamCreated")
    }

    override fun onStreamDestroyed(publisherKit: PublisherKit, stream: Stream) {
        Log.i(LOG_TAG, "Publisher onStreamDestroyed")
        cleanupPublisher()
        if (subscriber?.stream?.streamId == stream.streamId) {
            cleanupSubscriber()
        }
    }

    override fun onError(publisherKit: PublisherKit, opentokError: OpentokError) {
        Log.e(LOG_TAG, "Publisher error: " + opentokError.message)
    }

    /*** SUBSCRIBER LISTENER ***/
    override fun onConnected(subscriber: SubscriberKit) {
        addSubscriberAndPublisherViews()
    }

    override fun onDisconnected(p0: SubscriberKit) {

    }

    override fun onError(p0: SubscriberKit, error: OpentokError?) {
        Log.e(LOG_TAG, "Subscriber error: " + error?.message)
    }

    /** Custom functions **/
    fun initializeSession(sessionId: String, token: String, apiKey: String) {
        Log.i(LOG_TAG, "initializeSession")
        session = Session.Builder(context, apiKey, sessionId).build()
        session?.setSessionListener(this)
        session?.connect(token) // triggers onConnected(session: Session) once connected
    }

    fun endSession(): String {
        session?.disconnect()
        return ""
    }

    fun publishStream(sessionId: String, token: String, apiKey: String, name: String?): String {
        Log.i(LOG_TAG, "publishStream")
        publisher = Publisher.Builder(context).name(name).build()
        publisher?.setPublisherListener(this)

        if (session == null || session?.connection == null) {
            Log.i(LOG_TAG, "initializeSession from publish")

            initializeSession(sessionId = sessionId, token = token, apiKey = apiKey)
        } else {
            doPublish()
        }
        return ""
    }

    fun unPublishStream(): String {
        Log.i(LOG_TAG, "---------")
        Log.i(LOG_TAG, "unPublishStream")
        session?.unpublish(publisher)

        cleanupPublisher()
        cleanupSubscriber()

        session?.disconnect()
        session = null
        Log.i(LOG_TAG, "---------")
        return ""
    }

    private fun cleanupPublisher() {
        Log.i(LOG_TAG, "cleanupPublisher")

        nativeView.publisherView.removeAllViews()
        publisher = null
    }

    private fun cleanupSubscriber() {
        Log.i(LOG_TAG, "cleanupSubscriber")

        nativeView.subscriberView.removeAllViews()
        subscriber = null
    }

    private fun doPublish() {
        Log.i(LOG_TAG, "doPublish")
        val localPublisher = publisher
        val localSession = session
        if (localPublisher == null || localSession == null) {
            Log.i(LOG_TAG, "doPublish is not ready to publish")
            return
        }

        // attach publisher to session.
        if (localPublisher.session.sessionId != localSession.sessionId) {
            localSession.publish(localPublisher)
        }

        addSubscriberAndPublisherViews()
    }

    /**
     * Publisher and subscriber views are added at same and in the order of subscriber first
     * and publisher later. This is to fix issue with overlap where the publisher view is overlapped
     * if it's added before subscriber view.
     */
    private fun addSubscriberAndPublisherViews() {
        val localPublisher = publisher
        val localSubscriber = subscriber

        if (localPublisher == null || localSubscriber == null) {
            Log.i(LOG_TAG, "addPublisherAndSubscriberViews is not ready yet")
            return
        }

        // ORDER MATTERS until this is fixed in a proper way - see comment of this method.
        // add subscriber view
        with(nativeView.subscriberView) {
            removeAllViews()
            addView(localSubscriber.view)
        }

        // add publisher view
        with(nativeView.publisherView) {
            removeAllViews()
            addView(localPublisher.view)
        }
    }
}