import Flutter
import UIKit

private var factory: FLNativeViewFactory?
public class SwiftVontagemethodchannelPlugin: NSObject, FlutterPlugin {

    
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "VonageChannelPlugin", binaryMessenger: registrar.messenger())
    let instance = SwiftVontagemethodchannelPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
    
    factory = FLNativeViewFactory(messenger: registrar.messenger())
    if let unwrapped = factory {
        registrar.register(unwrapped, withId: "flutter-vonage-video-publisher")
    }
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    if let unwrapped = factory {
        guard let nativeView = unwrapped.nativeView else {
            return result(FlutterError(code: "notInitalisaed", message: "", details: ""))
        }
        let args = call.arguments as! Dictionary<String, Any>
              if call.method == "initSession" {
                let sessionId: String = args["sessionId"] as! String
                let token: String = args["token"] as! String
                let apiKey: String = args["apiKey"] as! String
                nativeView.initSession(sessionId: sessionId, token: token, apiKey: apiKey, result: result)
              } else if call.method == "endSession" {
                nativeView.endSession(result: result)
              } else if call.method == "publishStream" {
                let name: String = args["name"] as! String
                let viewId: Int = args["viewId"] as! Int
                let sessionId: String = args["sessionId"] as! String
                let token: String = args["token"] as! String
                let apiKey: String = args["apiKey"] as! String
                nativeView.publishStream(name: name, viewId: viewId, sessionId: sessionId, token: token, apiKey: apiKey, result: result)
              } else if call.method == "unpublishStream" {
                nativeView.unpublishStream(result: result)
              } else {
                result(FlutterMethodNotImplemented)
            }
    }
    
    result(FlutterError(code: "notInitalisaed", message: "", details: ""))
  }
}
