//
//  FLNativeViewFactory.swift
//  vontagemethodchannel
//
//  Created by Sree Harsha P on 23/05/21.
//

import Flutter
import UIKit
import OpenTok

// *** Fill the following variables using your own Project info  ***
// ***            https://tokbox.com/account/#/                  ***
// Replace with your OpenTok API key
//let kApiKey = "47234344"
//// Replace with your generated session ID
//let kSessionId = "1_MX40NzIzNDM0NH5-MTYyMTgzNzA5ODAwNH5YVG1YdjdiOFFJZ2l4dWhhMS9RcEhtK21-fg"
//// Replace with your generated token
//let kToken = "T1==cGFydG5lcl9pZD00NzIzNDM0NCZzaWc9ZjVjMzAy®ZjE0ZGY3N2FlNjMxZGY5YTZjZjU5NmUyYTkxMzRjZmQ2MTpzZXNzaW9uX2lkPTFfTVg0ME56SXpORE0wTkg1LU1UWXlNVGd6TnpBNU9EQXdOSDVZVkcxWWRqZGlPRkZKWjJsNGRXaGhNUzlSY0VodEsyMS1mZyZjcmVhdGVfdGltZT0xNjIxODM3MDk4Jm5vbmNlPTAuMzIxMDYxMjgxMjY0MDI5JnJvbGU9bW9kZXJhdG9yJmV4cGlyZV90aW1lPTE2MjE4Mzg4OTgmaW5pdGlhbF9sYXlvdXRfY2xhc3NfbGlzdD0="

class FLNativeViewFactory: NSObject, FlutterPlatformViewFactory {
    private var messenger: FlutterBinaryMessenger
    var nativeView: FLNativeView?
    var token: String?
    
    init(messenger: FlutterBinaryMessenger) {
        self.messenger = messenger
        super.init()
    }

    func create(
        withFrame frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?
    ) -> FlutterPlatformView {
        nativeView = FLNativeView(
            frame: frame,
            viewIdentifier: viewId,
            arguments: args,
            binaryMessenger: messenger)
        return nativeView!
    }
    
    
}

class FLNativeView: NSObject, FlutterPlatformView {
    fileprivate var _view: UIView
    
    var session: OTSession?
    var publisher: OTPublisher?
    
    var subscriber: OTSubscriber?
    var token: String?

    func initSession(sessionId: String, token: String, apiKey: String, result: FlutterResult) {
        print("session", sessionId, "token", token, "apikey", apiKey)
        session = OTSession(apiKey: apiKey, sessionId: sessionId, delegate: self)
        self.token = token
        doConnect()
        result("")
      }
    
    func endSession(result: FlutterResult) {
         session?.disconnect(nil)
         result("")
     }
    
    func publishStream(
        name: String,
        viewId: Int,
        sessionId: String,
        token: String,
        apiKey: String,
        result: FlutterResult) {
          let settings = OTPublisherSettings()
        // settings.name = UIDevice.current.name
        settings.name = name
        publisher = OTPublisher(delegate: self, settings: settings)

        if(session == nil || session?.connection == nil) {
            initSession(sessionId: sessionId, token: token, apiKey: apiKey, result: result)
        } else {
            doPublish()
        }
        result("")
  }

  func unpublishStream(result: FlutterResult) {
        guard (publisher?.view) != nil else {
          return
        }
        
        if(publisher != nil) {
            session?.unpublish(publisher!, error: nil)
        }
          
        cleanupPublisher()
        cleanupSubscriber()
    
        var error: OTError?
        session?.disconnect()
        session = nil
    
        result("")
    }
    
    init(
        frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?,
        binaryMessenger messenger: FlutterBinaryMessenger?
    ) {
        _view = UIView()
        _view.bounds = frame
        super.init()
    }

    func view() -> UIView {
        return _view
    }
    
    /**
     * Asynchronously begins the session connect process. Some time later, we will
     * expect a delegate method to call us back with the results of this action.
     */
    fileprivate func doConnect() {
        print("doConnect")

        var error: OTError?
        defer {
            processError(error)
        }
        if(token != nil) {
            session?.connect(withToken: token!, error: &error)
        }
    }
    
    /**
     * Sets up an instance of OTPublisher to use with this session. OTPubilsher
     * binds to the device camera and microphone, and will provide A/V streams
     * to the OpenTok session.
     */
    fileprivate func doPublish() {
        print("doPublish")
        var error: OTError?
        defer {
            processError(error)
        }
        
        guard let pub = publisher else {
            return
        }
        
        session?.publish(pub, error: &error)
        
        let publisherWidth = 120
        let publisherHeight = 240
        
    
        if let pubView = pub.view {
            let x = _view.bounds.width - CGFloat(publisherWidth)
            let y = _view.bounds.height - CGFloat(publisherHeight)

            pubView.frame = CGRect(x: x, y: y, width: CGFloat(publisherWidth), height: CGFloat(publisherHeight))
            _view.addSubview(pubView)
        }
        
        if let subView = subscriber?.view {
            _view.addSubview(subView)
            _view.bringSubviewToFront(subView)
        }
    }
    
    /**
     * Instantiates a subscriber for the given stream and asynchronously begins the
     * process to begin receiving A/V content for this stream. Unlike doPublish,
     * this method does not add the subscriber to the view hierarchy. Instead, we
     * add the subscriber only after it has connected and begins receiving data.
     */
    fileprivate func doSubscribe(_ stream: OTStream) {
        print("doSubscribe")

        var error: OTError?
        defer {
            processError(error)
        }
        subscriber = OTSubscriber(stream: stream, delegate: self)
        
        session?.subscribe(subscriber!, error: &error)
    }
    
    fileprivate func cleanupSubscriber() {
        subscriber?.view?.removeFromSuperview()
        subscriber = nil
    }
    
    fileprivate func cleanupPublisher() {
        publisher?.view?.removeFromSuperview()
        publisher = nil
    }
    
    fileprivate func processError(_ error: OTError?) {
        print("processError", error.debugDescription)

        if let err = error {
            DispatchQueue.main.async {
                let controller = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//                self._view.present(controller, animated: true, completion: nil)
                // send back error
            }
        }
    }
}


// MARK: - OTSession delegate callbacks
extension FLNativeView: OTSessionDelegate {
    
    func sessionDidConnect(_ session: OTSession) {
        print("Session connected")
        doPublish()
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("Session disconnected")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("Session streamCreated: \(stream.streamId)")
        if subscriber == nil {
            doSubscribe(stream)
        }
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("Session streamDestroyed: \(stream.streamId)")
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanupSubscriber()
            cleanupPublisher()
        }
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("session Failed to connect: \(error.localizedDescription)")
    }
    
}

// MARK: - OTPublisher delegate callbacks
extension FLNativeView: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        print("Publishing")
    }
    
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        cleanupPublisher()
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanupSubscriber()
        }
    }
    
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Publisher failed: \(error.localizedDescription)")
    }
}

// MARK: - OTSubscriber delegate callbacks
extension FLNativeView: OTSubscriberDelegate {
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        print("subscriberDidConnect")
        if let subsView = subscriber?.view {
            subsView.frame = CGRect(x: 0, y: 0, width: _view.bounds.width, height: _view.bounds.height)
            _view.addSubview(subsView)
        }
        
        if let publisherView = publisher?.view {
            _view.bringSubviewToFront(publisherView)
        }
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber failed: \(error.localizedDescription)")
    }
}
