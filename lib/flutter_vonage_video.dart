import 'dart:async';

import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class FlutterVonageVideo {
  static const MethodChannel _channel =
      const MethodChannel('VonageChannelPlugin');

  static Future<bool> initSession(
      String sessionId, String token, String apiKey) async {
    print('initSession');
    // for iOS simulator hard code `bool havePermissions = true` as cam and audio
    // is not supported on simulators.
    bool havePermissions = await checkPermissions();
    print('havePermissions $havePermissions');
    if (havePermissions) {
      try {
        print('trying to conect to initSession');
        await _channel.invokeMethod('initSession',
            {"sessionId": sessionId, "token": token, "apiKey": apiKey});
        return true;
      } on PlatformException {
        print('error');
        return false;
      }
    }
    return false;
  }

  static Future<String> endSession() async {
    print('endSession');
    try {
      return await _channel.invokeMethod('endSession', {});
    } on PlatformException {
      return "error";
    }
  }

  static Future<String> publishStream(String sessionId, String token,
      String apiKey, String name, int viewId) async {
    try {
      return await _channel.invokeMethod('publishStream', {
        "name": name,
        "viewId": viewId,
        "sessionId": sessionId,
        "token": token,
        "apiKey": apiKey
      });
    } on PlatformException {
      return "error";
    }
  }

  static Future<String> unpublishStream() async {
    try {
      return await _channel.invokeMethod('unpublishStream', {});
    } on PlatformException {
      return "error";
    }
  }

  static Future<bool> checkPermissions() async {
    bool cameraGranted = await Permission.camera.request().isGranted;
    bool cameraDenied = false;
    if (!cameraGranted) {
      cameraDenied = await Permission.camera.isPermanentlyDenied;
    }
    bool microphoneGranted = await Permission.microphone.request().isGranted;
    bool microphoneDenied = false;
    if (!microphoneGranted) {
      microphoneDenied = await Permission.microphone.isPermanentlyDenied;
    }

    if (cameraDenied || microphoneDenied) {
      openAppSettings();
    }
    if (cameraGranted && microphoneGranted) {
      return true;
    }
    return false;
  }
}
